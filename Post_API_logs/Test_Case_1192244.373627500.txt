Endpoint is :
https://reqres.in/api/users

Request Body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response Date is :
Sun, 25 Feb 2024 13:52:43 GMT

Response Body is :
{"name":"morpheus","job":"leader","id":"82","createdAt":"2024-02-25T13:52:43.252Z"}

