package TestPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test_Case_2 extends RequestBody {

	public static void executor() throws IOException {

		File dir_name = Utility.CreateLogDirectory("Post_API_logs");

		String requestBody = RequestBody.req_post_tc("Post_TC2");
		String Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		Response response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				requestBody, Endpoint);

		Utility.evidenceFileCreater(Utility.testLogName("Test_Case_2"), dir_name, Endpoint, requestBody, response.getHeader("Date"),
				response.getBody().asString());

		// Extract the response parameters
		int statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		String res_name = res_body.jsonPath().getString("name");
		String res_job = res_body.jsonPath().getString("job");
		String res_id = res_body.jsonPath().getString("id");
		String res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);

		// Set the expected results
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// System.out.println(req_name);
		// System.out.println(req_job);

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		 //System.out.println(expecteddate);
		// System.out.println(res_createdAt);

		// Validate the response parameters
		Assert.assertEquals(statuscode, 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);

		System.out.println("POST API SUCCESSFULLY VALIDATED...!!!!");

	}

}
/*
 * This program/framework is about to : trigger api and save response or
 * evidence in text file create folder--->create file inside the
 * folder--->write, save and exit file
 * Evidence save: Response Body, Request Body, Header(Date)
 * Run multiple test cases using runner/driver class
 * 
 */