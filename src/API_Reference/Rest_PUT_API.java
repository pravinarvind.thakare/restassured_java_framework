package API_Reference;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Rest_PUT_API {

	public static void main(String[] args) {
		//Step 1 : Collect all needed information and save it into local variable
		String req_Body = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		String hostname = "https://reqres.in";
		String resource = "/api/users/2";
		String headername = "Content-Type";
		String headervalue = "application/json";
		
		//Step 2 : Build the request specification using RequestSpecification class
		RequestSpecification req_spec = RestAssured.given();
		
		//Step 2.1 : Set request header
		req_spec.header(headername, headervalue);
		
		//Step 2.2 : Set request body
		req_spec.body(req_Body);
		
		//Step 3 : Sent the API request
		Response response = req_spec.put(hostname + resource);
		int res_statusCode = response.statusCode();
		//System.out.println(res_statusCode);
		
		//Step 4 : Parse the response body
		ResponseBody res_Body = response.getBody();
		
		String res_name = res_Body.jsonPath().getString("name");
		//System.out.println(res_name);
		String res_job = res_Body.jsonPath().getString("job");
		//System.out.println(res_job);
		String res_updatedAt = res_Body.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0,11);
		//System.out.println(res_updatedAt);
		
		//Step 5 : Validate the response body
		//Step 5.1 : Parse the request body and save it into local varibale
		JsonPath jsp_Req = new JsonPath(req_Body);
		String req_name = jsp_Req.getString("name");
		String req_job = jsp_Req.getString("job");
		
		//Step 5.2 : Generate expected date 
		LocalDateTime currentDate = LocalDateTime.now();
		String expectedDate = currentDate.toString().substring(0,11);
		
		//Step 5.3 : Use TestNG's Assert
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expectedDate);
		Assert.assertEquals(res_statusCode,200);
		System.out.println("PUT API successfullt validated...!");
		
		
		

	}

}
