package API_Reference;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Rest_PATCH_API {

	public static void main(String[] args) {
		//Step 1 : Collect all needed information and save it into local variable
		String Req_body = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		String hostname = "https://reqres.in";
		String resource = "/api/users/2";
		String headername = "Content-Type";
		String headervalue = "application/json";
		
		//Step 2 : Build request specification class using RequestSpecification class
		RequestSpecification req_spec = RestAssured.given();
		
		//Step 2.1 : Set request header
		req_spec.header(headername , headervalue);
		
		//Step 2.2 : Set request body
		req_spec.body(Req_body);
		
		//Step 3 : Send the API request
		Response response = req_spec.patch(hostname + resource);
		
		//Step 4 : Parse the response body
		ResponseBody Res_body = response.getBody();
		
		String res_name = Res_body.jsonPath().getString("name");
		//System.out.println(res_name);
		String res_job = Res_body.jsonPath().getString("job");
		//System.out.println(res_job);
		String res_updatedAt = Res_body.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0,11);
		//System.out.println(res_updatedAt);
		
		//Step 5 : Validate the response body
		//Step 5.1 : Parse request body and save it into local variable
		JsonPath jsp_Req = new JsonPath(Req_body);
		String req_name = jsp_Req.getString("name");
		String req_job = jsp_Req.getString("job");
				
		//Step 5.2 : Generate expected date
		LocalDateTime currentDate = LocalDateTime.now();
		String expectedDate = currentDate.toString().substring(0,11);
		//System.out.println(expectedDate);
		
		//Step 5.3 : Use TestNG's Assert
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expectedDate);
		System.out.println("PATCH API Successfully Validated..!!!");

		
	}
	
}
