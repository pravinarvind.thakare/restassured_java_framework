package API_Reference;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Rest_POST_API {

	public static void main(String[] args) {
		//Step 1 : Collect all needed information and save it into local variable
		String req_body = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";
		String hostname = "https://reqres.in";
		String resource = "/api/users";
		String headername = "Content-Type";
		String headervalue = "application/json";
		
		//Step 2 : Build the request specification using RequestSpecification class
		RequestSpecification requestSpec = RestAssured.given();
		
		//Step 2.1 : Set request header
		requestSpec.header(headername, headervalue);
		
		//Step 2.2 : Set request body
		requestSpec.body(req_body);
		
		//Step 3 : Send the API request
		Response response = requestSpec.post(hostname + resource);
		//System.out.println(response.getBody().asString());
		int res_StatusCode = response.statusCode();
		//System.out.println("Status Code:"+res_StatusCode);
		
		//Step 4 : Parse the response body
		ResponseBody res_body = response.getBody();
		
		String res_name = res_body.jsonPath().getString("name");
		//System.out.println("Name:"+ res_name);
		String res_job = res_body.jsonPath().getString("job");
		//System.out.println("Job:"+res_job);
		String res_id = res_body.jsonPath().getString("id");
		//System.out.println("Id:"+res_id);
		String res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0,11);
		//System.out.println("Created Date:"+res_createdAt);
		//String res_statusCode = res_body.sta
		
		//Step 5 : Validate the response body
		//Step 5.1 :  Parse request body and save it into local variable
		JsonPath jspReq = new JsonPath(req_body);
		String req_name = jspReq.getString("name");
		String req_job = jspReq.getString("job");
		
		//Step 5.2 : Generate expected date
		LocalDateTime currentDate = LocalDateTime.now();
		String expectedDate = currentDate.toString().substring(0,11);
		//System.out.println(expectedDate);
		
 		//Step 5.3 : Use TestNG's Assert
		Assert.assertEquals(res_StatusCode, 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt,expectedDate);
		System.out.println("POST API successfully validated");
		
				
	}

}
